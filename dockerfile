FROM nginx:1.23.3-alpine
WORKDIR /app

COPY ./img ./static-site/img
COPY ./css ./static-site/css
COPY ./js ./static-site/js
COPY ./index.html ./static-site/index.html
COPY ./load.html ./static-site/load.html
COPY ./video.html ./static-site/video.html
COPY ./static-site.conf /etc/nginx/conf.d/static-site.conf

CMD ["nginx" "-g" "daemon off;"]