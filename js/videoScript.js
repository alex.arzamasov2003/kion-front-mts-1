// мой код
const download = async () => {
  let url = await getDownloadURL(
    document.getElementById("name").value.replace(/(.*)\.[^.]+$/, "$1")
  );
  if(url != undefined) {
    const html =
    '<video width="1080" controls autoplay> <source src="' +
    url +
    '" type="video/mp4" /> />';
  document.getElementById("videoPlayer").innerHTML = html;
  console.log("OK");
  }
};

document.getElementById("video_btn").onclick = async () => {
  download();
};

document.getElementById("list_btn").onclick = async () => {
  getAllVideos();
};

document.getElementById("delete_btn").onclick = async () => {
  deleteAllVideos();
};

document.getElementById("list_btn_down").onclick = async () => {
  document.getElementById("list").innerText = "";
};

const getDownloadURL = async (name) => {
  let line = "http://91.185.84.135:4001/s3urlGET/" + name;
  if (name !== "") {
    const response = await fetch(line);
    let url = await response.json();
    return url;
  }
};

const getAllVideos = async () => {
  let line = "http://91.185.84.135:4001/files";
  let response = await (await fetch(line)).json();
  let files = response.files;
  let text = "";
  for (let i = 0; i < files.length; ++i) {
    text +=
      '<div class="mt-3">' +
      files[i].fileName +
      '</div>';
  }
  document.getElementById("list").innerHTML = text;
};

const deleteAllVideos = async () => {
  let line = "http://91.185.84.135:4001/files";
  await fetch(line, {
    method: "DELETE",
  })
    .then((response) => {
      console.log(response.status);
    })
    .catch((error) => {
      console.error(error);
    });
  console.log("Success");
  document.getElementById("list").innerHTML = "";
};
