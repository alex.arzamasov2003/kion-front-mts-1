/* Вurger menu */
const isMobile = {
  Android: function () {
    return navigator.userAgent.match(/Android/i);
  },

  BlackBerry: function () {
    return navigator.userAgent.match(/BlackBerry/i);
  },

  IOS: function () {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },

  Opera: function () {
    return navigator.userAgent.match(/Opera Mini/i);
  },

  Windows: function () {
    return navigator.userAgent.match(/IEMobile/i);
  },

  any: function () {
    return (
      isMobile.Android() ||
      isMobile.BlackBerry() ||
      isMobile.IOS() ||
      isMobile.Opera() ||
      isMobile.Windows()
    );
  },
};

if (isMobile.any()) {
  document.body.classList.add("_touch");

  let menuArrows = document.querySelectorAll(".menu_arrow");

  if (menuArrows.length > 0) {
    for (let index = 0; index < menuArrows.length; index++) {
      const menuArrow = menuArrows[index];
      menuArrow.addEventListener("click", function (e) {
        menuArrow.parentElement.classList.toggle("_active");
      });
    }
  }
} else {
  document.body.classList.add("_pc");
}

const iconMenu = document.querySelector(".menu_icon");
const menuBody = document.querySelector(".menu_body");

if (iconMenu) {
  iconMenu.addEventListener("click", function (e) {
    document.body.classList.toggle("_lock");
    iconMenu.classList.toggle("_active");
    menuBody.classList.toggle("_active");
  });
}

/* Smooth slide navigation */
const menuLinks = document.querySelectorAll("a[data-goto]");
if (menuLinks.length > 0) {
  menuLinks.forEach((menuLink) => {
    menuLink.addEventListener("click", onMenuLinkClick);
  });

  function onMenuLinkClick(e) {
    const menuLink = e.target;
    if (
      menuLink.dataset.goto &&
      document.querySelector(menuLink.dataset.goto)
    ) {
      const gotoBlock = document.querySelector(menuLink.dataset.goto);
      const gotoBlockValue =
        gotoBlock.getBoundingClientRect().top +
        pageYOffset -
        document.querySelector("header").offsetHeight;

      if (iconMenu.classList.contains("_active")) {
        document.body.classList.remove("_lock");
        iconMenu.classList.remove("_active");
        menuBody.classList.remove("_active");
      }

      window.scrollTo({
        top: gotoBlockValue,
        behavior: "smooth",
      });
      e.preventDefault();
    }
  }
}

/* Load */
const fileTempl = document.getElementById("file-template"),
  imageTempl = document.getElementById("image-template"),
  empty = document.getElementById("empty");

// use to store pre selected files
let FILES = {};
let cnt = 0;

// check if file is of type image and prepend the initialied
// template to the target element
function addFile(target, file) {
  const isImage = file.type.match("image.*");

  const clone = isImage
    ? imageTempl.content.cloneNode(true)
    : fileTempl.content.cloneNode(true);

  clone.querySelector("h1").textContent = file.name;
  clone.querySelector("li").id = cnt;
  clone.querySelector(".delete").dataset.target = cnt;
  clone.querySelector(".size").textContent =
    file.size > 1024
      ? file.size > 1048576
        ? Math.round(file.size / 1048576) + "mb"
        : Math.round(file.size / 1024) + "kb"
      : file.size + "b";

  isImage &&
    Object.assign(clone.querySelector("img"), {
      id: cnt,
      alt: file.name,
    });

  empty.classList.add("hidden");
  target.prepend(clone);

  FILES[cnt] = file;
  cnt += 1;
}

const gallery = document.getElementById("gallery"),
  overlay = document.getElementById("overlay");

// click the hidden input of type file if the visible button is clicked
// and capture the selected files
const hidden = document.getElementById("hidden-input");
document.getElementById("button").onclick = () => hidden.click();
hidden.onchange = (e) => {
  for (const file of e.target.files) {
    addFile(gallery, file);
  }
};

// use to check if a file is being dragged
const hasFiles = ({ dataTransfer: { types = [] } }) =>
  types.indexOf("Files") > -1;

// use to drag dragenter and dragleave events.
// this is to know if the outermost parent is dragged over
// without issues due to drag events on its children
let counter = 0;

// reset counter and append file to gallery when file is dropped
function dropHandler(ev) {
  ev.preventDefault();
  for (const file of ev.dataTransfer.files) {
    addFile(gallery, file);
    overlay.classList.remove("draggedover");
    counter = 0;
  }
}

// only react to actual files being dragged
function dragEnterHandler(e) {
  e.preventDefault();
  if (!hasFiles(e)) {
    return;
  }
  ++counter && overlay.classList.add("draggedover");
}

function dragLeaveHandler(e) {
  1 > --counter && overlay.classList.remove("draggedover");
}

function dragOverHandler(e) {
  if (hasFiles(e)) {
    e.preventDefault();
  }
}

// event delegation to caputre delete events
// fron the waste buckets in the file preview cards
gallery.onclick = ({ target }) => {
  if (target.classList.contains("delete")) {
    const ou = target.dataset.target;
    document.getElementById(ou).remove(ou);
    gallery.children.length === 1 && empty.classList.remove("hidden");
    delete FILES[ou];
  }
};

// clear entire selection
document.getElementById("cancel").onclick = () => {
  while (gallery.children.length > 0) {
    gallery.lastChild.remove();
  }
  FILES = {};
  cnt = 0;
  empty.classList.remove("hidden");
  gallery.append(empty);
};

//мой код
const getUploadURL = async (name) => {
  let line = "http://91.185.84.135:4001/s3urlPUT/" + name;
  const response = await fetch(line);
  let url = await response.json();
  return url;
};

const uploadFile = async (file) => {
  document.getElementById("dialog").innerText =
    "Статус загрузки в хранилище файла '" + file.name + "' - не загружено\n";

  let url = await getUploadURL(file.name.replace(/(.*)\.[^.]+$/, "$1"));
  await fetch(url, {
    method: "PUT",
    body: file,
  })
    .then((response) => {
      console.log(response.text());
      document.getElementById("dialog").innerText =
        "Статус загрузки в хранилище файла '" +
        file.name +
        "' - загрузка завершена\n";
    })
    .catch((error) => {
      console.error(error);
    });
  console.log("Success");
};

document.getElementById("submit").onclick = async () => {
  for (let i = 0; i < cnt; ++i) {
    await uploadFile(FILES[i]);
  }
  while (gallery.children.length > 0) {
    gallery.lastChild.remove();
  }
  FILES = {};
  cnt = 0;
  empty.classList.remove("hidden");
  gallery.append(empty);
  document.getElementById("dialog").innerText = "Все загрузки завершены";
};
